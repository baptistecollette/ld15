﻿/**
 * \file      ChangeToolTip.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de gérer une tour sur le terrain
 */

using UnityEngine;
using System.Collections;

public class TowerOnTheMap : MonoBehaviour {

	public GameObject tile;

	private Tower tower;
	
	public Sprite arrow;
	public Sprite cannon;
	public Sprite death;
	public Sprite fire;
	public Sprite ice;
	
	private float heightTile;
	private float widthTile;
	
	private bool onTheMap;

	// Use this for initialization
	void Start () 
	{
		onTheMap = false;

		widthTile = 2f * tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.x;
		heightTile = 2f * tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!onTheMap && tower!=null)
		{
			switch(tower.TowerType)
			{
			case(TowerTypes.ARROW_TOWER):
				this.gameObject.GetComponent<SpriteRenderer> ().sprite = arrow;
				break;
			case(TowerTypes.CANNON_TOWER):
				this.gameObject.GetComponent<SpriteRenderer> ().sprite = cannon;
				break;
			case(TowerTypes.DEATH_TOWER):
				this.gameObject.GetComponent<SpriteRenderer> ().sprite = death;
				break;
			case(TowerTypes.FIRE_TOWER):
				this.gameObject.GetComponent<SpriteRenderer> ().sprite = fire;
				break;
			case(TowerTypes.ICE_TOWER):
				this.gameObject.GetComponent<SpriteRenderer> ().sprite = ice;
				break;
			default:
				Debug.Log("Ce type de tour n'existe pas ... Vous ne devriez pas etre ici !");
				break;
			}

			this.transform.position = new Vector3(tower.Tile.PositionYOnMap * widthTile,-tower.Tile.PositionXOnMap * heightTile ,10f);

			onTheMap = true;

		}
	}

	public void SetTower(Tower _tower)
	{
		tower = _tower;
	}

	public Tower GetTower()
	{
		return tower;
	}
}
