﻿using UnityEngine;
using System.Collections;

public class ProjectileOnMap : MonoBehaviour {

	private GameObject target;
	
	public Sprite arrow;
	public Sprite cannon;
	public Sprite death;
	public Sprite fire;
	public Sprite ice;

	private Vector3 startPosition;

	private bool initialized = false;

	private float counter;

	private TowerTypes typeTower = TowerTypes.NULL_TOWER;

	// Use this for initialization
	void Start () 
	{
		counter = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!initialized)
		{
			if(typeTower!=TowerTypes.NULL_TOWER)
			{
				initialized=true;
				this.gameObject.transform.position = startPosition;
				
				switch(typeTower)
				{
				case(TowerTypes.ARROW_TOWER):
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = arrow;
					break;
				case(TowerTypes.CANNON_TOWER):
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = cannon;
					break;
				case(TowerTypes.DEATH_TOWER):
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = death;
					break;
				case(TowerTypes.FIRE_TOWER):
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = fire;
					break;
				case(TowerTypes.ICE_TOWER):
					this.gameObject.GetComponent<SpriteRenderer> ().sprite = ice;
					break;
				default:
					Debug.Log("Ce type de tour n'existe pas ... Vous ne devriez pas etre ici !");
					break;
				}
			}

		}
		else
		{
			Vector3 difference = target.transform.position-this.gameObject.transform.position;
			if(counter<0.5f && difference.magnitude>0.1f)
			{
				float k = counter/0.5f;



				this.gameObject.transform.position = this.gameObject.transform.position+k*difference;

				float rotZ = Mathf.Atan2 (difference.y,difference.x)*Mathf.Rad2Deg;
				this.gameObject.transform.rotation = Quaternion.Euler(0f,0f,rotZ);
					
				counter += Time.deltaTime;
			}
			else
			{
				Destroy(this.gameObject);
			}
		}
	}

	public void SetTarget(GameObject _target)
	{
		target = _target;
	}

	public GameObject GetTarget()
	{
		return target;
	}

	public void SetStartPosition(Vector3 _startPosition)
	{
		startPosition = _startPosition;
	}

	public Vector3 GetStartPosition()
	{
		return startPosition;
	}

	public void SetTypeTower(TowerTypes _typeTower)
	{
		typeTower = _typeTower;
	}

	public TowerTypes GetTypeTower ()
	{
		return typeTower;
	}
}
