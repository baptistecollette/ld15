﻿/**
 * \file      LinkTileGameObject.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de relier le GameObject au Tile C#
 */

using UnityEngine;
using System.Collections;

public class LinkTileGameObject : MonoBehaviour {

	private Tile tile;
	private int column;
	private int row;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetTile(Tile _tile)
	{
		tile = _tile;
	}

	public Tile GetTile()
	{
		return tile;
	}

	public void SetColumn(int _column)
	{
		column = _column;
	}
	
	public int GetColumn()
	{
		return column;
	}

	public void SetRow(int _row)
	{
		row = _row;
	}
	
	public int GetRow()
	{
		return row;
	}
}
