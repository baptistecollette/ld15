﻿/**
 * \file      ChangeToolTip.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de changer le tooltip quand on passe la souris sur un bouton de troupe
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ChangeToolTip : MonoBehaviour {


	private static GameObject toolTip;
	private static Text nameMonster;
	private static Text cost;
	private static Text income;
	private static Text description;

	// Use this for initialization
	void Start () 
	{
			
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void ChangeTheToolTip()
	{
		if(toolTip==null)
		{
			toolTip = GameObject.Find("MenuTroopsCreation").GetComponent<AddAToolTip>().GetToolTip();

			nameMonster = toolTip.transform.Find("NameTroop").GetComponent<Text>();
			cost = toolTip.transform.Find("Cost").GetComponent<Text>();
			income = toolTip.transform.Find("Income").GetComponent<Text>();
			description = toolTip.transform.Find("Description").GetComponent<Text>();
		}

		toolTip.SetActive (true);

		int numberOfTroop = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));

		switch(numberOfTroop)
		{
		case(1):
			nameMonster.text = "Petit Gobelin";
			cost.text = "10 larmes";
			income.text = "Revenu : 1";
			description.text = "Ce petit gobelin pourra tuer des gens. S'il n'est pas mort avant ...";
			break;
		case(2):
			nameMonster.text = "Orc";
			cost.text = "50 larmes";
			income.text = "Revenu : 5";
			description.text = "Créature démoniaque de base. Fiable et peu chère.";
			break;
		case(3):
			nameMonster.text = "Loup-Garou";
			cost.text = "70 larmes";
			income.text = "Revenu : 5";
			description.text = "A ne pas confondre avec le Rougarou, bien plus mortel.";
			break;
		case(4):
			nameMonster.text = "Harpie";
			cost.text = "150 larmes";
			income.text = "Revenu : 7";
			description.text = "Peu également servir de ventilateur lorsqu'il fait chaud.";
			break;
		case(5):
			nameMonster.text = "Vampire";
			cost.text = "200 larmes";
			income.text = "Revenu : 14";
			description.text = "Ne vous faites pas avoir s'il vous demande de \"saigner ici\".";
			break;
		case(6):
			nameMonster.text = "Cyclope";
			cost.text = "300 larmes";
			income.text = "Revenu : 20";
			description.text = "Ce demi-géant vous a à l'oeil. Et le bon.";
			break;
		case(7):
			nameMonster.text = "Démon";
			cost.text = "500 larmes";
			income.text = "Revenu : 20";
			description.text = "Rouges, grands, chauds et musclés. En un mot : d'enfer.";
			break;
		case(8):
			nameMonster.text = "Chevalier Noir";
			cost.text = "650 larmes";
			income.text = "Revenu : 40";
			description.text = "Qu'est ce qui a un cheval, qui est noir et qui fait peur ?";
			break;
		case(9):
			nameMonster.text = "Dragon";
			cost.text = "2000 larmes";
			income.text = "Revenu : -100";
			description.text = "Des écailles, des ailes. Il est le feu. Il est la mort.";
			break;
		default:
			Debug.Log ("Ce cas n'aurait pas du se produire ...");
			break;
		}

		//Make sure that the lines are not more than 23 characters
		description.text = AddBreakLine (description.text, 23);

	}

	public void DisableToolTip()
	{
		if(toolTip==null)
		{
			toolTip = GameObject.Find("MenuTroopsCreation").GetComponent<AddAToolTip>().GetToolTip();
			
			nameMonster = toolTip.transform.Find("NameTroop").GetComponent<Text>();
			cost = toolTip.transform.Find("Cost").GetComponent<Text>();
			description = toolTip.transform.Find("Description").GetComponent<Text>();
		}
		
		toolTip.SetActive (false);
	}

	private string AddBreakLine(string input, int lineLength)
	{
		string[] wordsList = input.Split (' ');
		int counter = 0;
		string finalString = "";

		for(int i=0; i<wordsList.Length;i++)
		{
			//If it will be larger than the defined line, then add a break line
			if(counter+wordsList[i].Length>lineLength)
			{
				finalString+="\n"+wordsList[i]+" ";
				counter=wordsList[i].Length;
			}
			//Else, add the current word and go ahead
			else
			{
				finalString+=wordsList[i]+" ";
				counter+=wordsList[i].Length;
			}
		}
		return finalString;
	}
}
