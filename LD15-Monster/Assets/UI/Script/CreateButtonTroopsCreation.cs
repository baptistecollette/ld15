﻿/**
 * \file      CreateButtonTroopsCreation.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de créer une grille de 3x3 boutons pour créer des troupes
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateButtonTroopsCreation : MonoBehaviour {

	public GameObject ButtonCreationTroop;

	// Use this for initialization
	void Start () 
	{
		//Get the dimensions of the button
		float widthButton = ButtonCreationTroop.GetComponent<RectTransform> ().sizeDelta.x;
		float heightButton = ButtonCreationTroop.GetComponent<RectTransform> ().sizeDelta.y;

		//Calculate the space between each button in each dimension
		float spaceBetweenButtonRow = (this.gameObject.GetComponent<RectTransform> ().sizeDelta.y - 3f * heightButton) / 4f;
		float spaceBetweenButtonColumn = (this.gameObject.GetComponent<RectTransform> ().sizeDelta.x - 3f * widthButton) / 4f;
		for(int i = 0;i<3;i++)//Rows
		{
			for(int j = 1;j<4;j++)//Columns
			{
				//Instantiate the button
				GameObject button = Instantiate(ButtonCreationTroop);
				//Set its parent and its name
				button.transform.SetParent(this.gameObject.transform);
				button.name = "ButtonCreationTroop"+(3*i+j).ToString();

				//Get its recttransform
				RectTransform buttonRectTransform = button.GetComponent<RectTransform>();
				//Set its position to (0,0)
				buttonRectTransform.anchoredPosition -= buttonRectTransform.anchoredPosition;
				//Set its position to have a nice matrix 
				float XPosition = (float)j*spaceBetweenButtonColumn+(float)(j-1)*widthButton-this.gameObject.GetComponent<RectTransform> ().sizeDelta.x/2f + widthButton/2f;
				float YPosition = (float)((2-i)+1)*spaceBetweenButtonRow+(float)(2-i)*heightButton-this.gameObject.GetComponent<RectTransform> ().sizeDelta.y/2f + heightButton/2f;
				buttonRectTransform.anchoredPosition += new Vector2(XPosition,YPosition);
				//Make sure that the scale is (1,1,1)
				buttonRectTransform.localScale = new Vector3(1f,1f,1f);
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
