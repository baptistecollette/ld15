﻿using UnityEngine;
using System.Collections;
#if !UNITY_ANDROID
#if UNITY_EDITOR
using UnityEditor;
#endif
#endif

public class QuitGame : MonoBehaviour {
	
	public void QuitterLeJeu()
	{
		#if !UNITY_ANDROID
		#if UNITY_EDITOR
		EditorApplication.isPlaying = false;
#endif
		#else
		Application.Quit();
		#endif
	}
	
}
