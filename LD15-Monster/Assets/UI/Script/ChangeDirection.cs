﻿/**
 * \file      ChangeDirection.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de savoir quelle direction est appuyée, et d'empecher que plusieurs boutons soient appuyés en meme temps
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ChangeDirection : MonoBehaviour {


	//Stores the buttons to change the direction
	private List<Button> listOfButtonsDirection;

	//Stores the current direction. 1 is North, 2 is West, 3 is East and 4 is South
	private int currentDirection;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			ChangeTheDirection(1);
		}
		if(Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow))
		{
			ChangeTheDirection(2);
		}
		if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			ChangeTheDirection(3);
		}
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			ChangeTheDirection(4);
		}
	}

	public void AddButtonToList(Button button)
	{
		if(listOfButtonsDirection==null)
		{
			listOfButtonsDirection = new List<Button>(){};
		}
		//Add the button to the list
		listOfButtonsDirection.Add (button);
		//Once the list has the 4 buttons, select the first one (North)
		if(listOfButtonsDirection.Count==4)
		{
			ChangeTheDirection(1);
		}
	}

	public void ChangeTheDirection (int newDirection)
	{
		if(newDirection<1 || newDirection>4)
		{
			Debug.Log("Erreur, vous ne pouvez pas aller dans cette direction ...");
		}
		else
		{
			foreach(Button b in listOfButtonsDirection)
			{
				int direction = int.Parse (b.gameObject.name.Substring (b.gameObject.name.Length - 1));
				if(direction == newDirection)
				{
					b.interactable = false;
					currentDirection = newDirection;
				}
				else
				{
					b.interactable = true;
				}
			}
		}
	}

	public int GetCurrentDirection()
	{
		return currentDirection;
	}


}


