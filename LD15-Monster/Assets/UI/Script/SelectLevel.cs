﻿using UnityEngine;
using System.Collections;

public class SelectLevel : MonoBehaviour {

	public string level;
	public string difficulty;

	public void ChangeLevel ()
	{
		if (level != null){
		GameManager.Instance.FileName = level;
		Application.LoadLevel("SceneUI");
		}
	}

	public void SetDifficulty()
	{
		switch (difficulty)
		{
		case "easy":
			GameManager.Instance.Difficulty = Difficulty.EASY;
			break;

		case "medium":
			GameManager.Instance.Difficulty = Difficulty.MEDIUM;
			break;

		case "hard":
			GameManager.Instance.Difficulty = Difficulty.HARD;
			break;
		}

	}
	
}
