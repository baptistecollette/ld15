﻿/**
 * \file      CreateMonsterClickButton.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de créer un monstre quand on clique sur le bouton
 */

using UnityEngine;
using System.Collections;

public class CreateMonsterClickButton : MonoBehaviour {

	public GameObject prefabMonster;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void CreateAMonster()
	{
		int numberOfTroop = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));
		int direction = GameObject.Find ("MenuChoiceDirection").GetComponent<ChangeDirection> ().GetCurrentDirection ();
		GameManager gameManager = GameManager.Instance;
		Road road = null;
		if(direction==1)
		{
			road = gameManager.UpperRoad;
		}
		else if(direction==2)
		{
			road = gameManager.LeftRoad;
		}
		
		else if(direction==3)
		{
			road = gameManager.RightRoad;
		}
		
		else if(direction==4)
		{
			road = gameManager.DownRoad;
		}
		else
		{
			Debug.Log("La route : "+direction.ToString()+" n'existe pas ...");
			return;
		}

		GameObject monster = null;
		GameObject map = GameObject.Find ("Map");

		//Instantiate a monster, set its parent and its position to (0,0,0)
		monster = GameObject.Instantiate(prefabMonster);
		monster.transform.SetParent(map.transform);
		monster.transform.position -= monster.transform.position;

		Monster monsterCode = null;
		MonsterOnTheMap monsterOnTheMap = null;

		switch(numberOfTroop)
		{
		case(1):
			//Create the C# monster
			monsterCode = Goblin.CreateMonster(road);

			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Gobelin");
			break;
		case(2):
			//Create the C# monster
			monsterCode = Orc.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Orc");
			break;
		case(3):
			//Create the C# monster
			monsterCode = Werewolf.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Werewolf");

			break;
		case(4):
			//Create the C# monster
			monsterCode = Harpy.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Harpy");

			break;
		case(5):
			//Create the C# monster
			monsterCode = Vampire.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Vampire");

			break;
		case(6):
			//Create the C# monster
			monsterCode = Cyclops.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Cyclops");
			break;
		case(7):
			//Create the C# monster
			monsterCode = Demon.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Demon");

			break;
		case(8):
			//Create the C# monster
			monsterCode = Deathknight.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Deathknight");

			break;
		case(9):
			//Create the C# monster
			monsterCode = Dragon.CreateMonster(road);
			
			//Gives to the sprite the Monster in C#
			monsterOnTheMap = monster.GetComponent<MonsterOnTheMap>();
			monsterOnTheMap.SetMonster(monsterCode);
			monsterOnTheMap.SetMonsterType("Dragon");

			break;
		default:
			Debug.Log ("Ce cas n'aurait pas du se produire ...");
			break;
		}

		monsterCode.GameObjectMonster = monster;
		//TODO Vérifier les sous, enlever les sous.
	}
}
