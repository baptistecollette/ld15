﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetGoldAndIncome : MonoBehaviour {

	public Text textGold;
	public Text textIncome;
	public Text textTimer;

	// Update is called once per frame
	void Update () {

		if (Badguy.Instance != null) {
			textGold.text = Badguy.Instance.TotalMoney.ToString();
			textIncome.text = Badguy.Instance.Income.ToString();
			int timeRemainingSeconds = GameManager.Instance.TimeRemainingSeconds;
			int sec = timeRemainingSeconds%60;
			int min = timeRemainingSeconds/60;
			textTimer.text = min + " mins " + sec + " s";
		}
	
	}
}
