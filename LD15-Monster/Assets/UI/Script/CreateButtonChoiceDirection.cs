﻿/**
 * \file      CreateButtonChoiceDirection.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de créer une grille de 4 boutons pour choisir la direction d'envoie des troupes
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateButtonChoiceDirection : MonoBehaviour {

	public GameObject ButtonChoiceDirection;

	// Use this for initialization
	void Start () 
	{
		//Get the dimensions of the button
		float widthButton = ButtonChoiceDirection.GetComponent<RectTransform> ().sizeDelta.x;
		float heightButton = ButtonChoiceDirection.GetComponent<RectTransform> ().sizeDelta.y;

		//Calculate the space between each button in each dimension
		float spaceBetweenButtonRow = (this.gameObject.GetComponent<RectTransform> ().sizeDelta.y - 3f * heightButton) / 4f;
		float spaceBetweenButtonColumn = (this.gameObject.GetComponent<RectTransform> ().sizeDelta.x - 3f * widthButton) / 4f;
		int counter = 1;
		for(int i = 0;i<3;i++)//Rows
		{
			for(int j = 1;j<4;j++)//Columns
			{
				if((i==0 && j==2)||(i==1 && j==1)||(i==1 && j==3)||(i==2 && j==2))
				{
					//Instantiate the button
					GameObject button = Instantiate(ButtonChoiceDirection);
					//Set its parent and its name
					button.transform.SetParent(this.gameObject.transform);
					button.name = "ButtonChoiceDirection"+(counter).ToString();


					//Get its recttransform
					RectTransform buttonRectTransform = button.GetComponent<RectTransform>();
					//Set its position to (0,0)
					buttonRectTransform.anchoredPosition -= buttonRectTransform.anchoredPosition;
					//Set its position to have a nice matrix 
					float XPosition = (float)j*spaceBetweenButtonColumn+(float)(j-1)*widthButton-this.gameObject.GetComponent<RectTransform> ().sizeDelta.x/2f + widthButton/2f;
					float YPosition = (float)((2-i)+1)*spaceBetweenButtonRow+(float)(2-i)*heightButton-this.gameObject.GetComponent<RectTransform> ().sizeDelta.y/2f + heightButton/2f;
					buttonRectTransform.anchoredPosition += new Vector2(XPosition,YPosition);
					//Make sure that the scale is (1,1,1)
					buttonRectTransform.localScale = new Vector3(1f,1f,1f);

					//Set the rotation
					if(i==0 && j==2)
					{
						buttonRectTransform.localRotation = Quaternion.Euler(new Vector3(0f,0f,90f));
					}
					else if(i==1 && j==1)
					{
						buttonRectTransform.localRotation = Quaternion.Euler(new Vector3(0f,0f,-180f));
					}
					else if(i==2 && j==2)
					{
						buttonRectTransform.localRotation = Quaternion.Euler(new Vector3(0f,0f,-90f));
					}
					this.gameObject.GetComponent<ChangeDirection>().AddButtonToList(button.GetComponent<Button>());
					counter++;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
