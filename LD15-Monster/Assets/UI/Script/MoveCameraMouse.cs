﻿/**
 * \file      MoveCameraMouse.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet à la caméra d'etre reliée à la souris
 */

using UnityEngine;
using System.Collections;

public class MoveCameraMouse : MonoBehaviour 
{
	private float limitXLeft;
	private float limitXRight;
	private float limitYBottom;
	private float limitYUp;
	private float limitZoomMax;
	private float limitZoomMin;
	private float movementSpeed;

	private bool cameraLocked;

	// Use this for initialization
	void Start () 
	{
		limitXLeft = -0f;
		limitXRight = 20f;
		limitYUp = 0.5f;
		limitYBottom = -18f;
		limitZoomMax = 3f;
		limitZoomMin = -3f;
		movementSpeed = 10f;
		cameraLocked = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!cameraLocked)
		{
			Vector3 MousePosition = Input.mousePosition;

			//Check if the camera needs to move on X axis
			if(MousePosition.x<(float)Screen.width/100f*5f && !(this.gameObject.transform.position.x<limitXLeft))
			{
				this.gameObject.transform.position -= new Vector3(movementSpeed*Time.deltaTime,0,0);
			}
			else if(MousePosition.x>(float)Screen.width/100f*95f && !(this.gameObject.transform.position.x>limitXRight))
			{
				this.gameObject.transform.position += new Vector3(movementSpeed*Time.deltaTime,0,0);
			}

			//Check if the camera needs to move on Y axis
			if(MousePosition.y<(float)Screen.height/100f*5f && !(this.gameObject.transform.position.y<limitYBottom))
			{
				this.gameObject.transform.position -= new Vector3(0,movementSpeed*Time.deltaTime,0);
			}
			else if(MousePosition.y>(float)Screen.height/100f*95f && !(this.gameObject.transform.position.y>limitYUp))
			{
				this.gameObject.transform.position += new Vector3(0,movementSpeed*Time.deltaTime,0);
			}
		}
		//Check the wheel for zoom purpose
		if(Input.GetAxis("Mouse ScrollWheel")>0 && this.gameObject.transform.position.z<limitZoomMax)
		{
			this.gameObject.transform.position += new Vector3(0,0,50f*movementSpeed*Time.deltaTime*Input.GetAxis("Mouse ScrollWheel"));
		}
		else if(Input.GetAxis("Mouse ScrollWheel")<0 && this.gameObject.transform.position.z>limitZoomMin)
		{
			this.gameObject.transform.position += new Vector3(0,0,50f*movementSpeed*Time.deltaTime*Input.GetAxis("Mouse ScrollWheel"));
		}
	}

	public bool GetCameraLocked()
	{
		return cameraLocked;
	}

	public void SetCameraLocked(bool _cameraLocked)
	{
		cameraLocked = _cameraLocked;
	}
}


