﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GestionButtonCreationTroop : MonoBehaviour {

	public Sprite gobelin;
	public Sprite orc;
	public Sprite werewolf;
	public Sprite harpy;
	public Sprite vampire;
	public Sprite cyclops;
	public Sprite demon;
	public Sprite deathknight;
	public Sprite dragon;

	private int money;


	// Use this for initialization
	void Start () 
	{
		int numberOfTroop = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));
		switch(numberOfTroop)
		{
		case(1):
			this.gameObject.GetComponent<Image>().sprite = gobelin;
			break;
		case(2):
			this.gameObject.GetComponent<Image>().sprite = orc;
			break;
		case(3):
			this.gameObject.GetComponent<Image>().sprite = werewolf;
			break;
		case(4):
			this.gameObject.GetComponent<Image>().sprite = harpy;
			break;
		case(5):
			this.gameObject.GetComponent<Image>().sprite = vampire;
			break;
		case(6):
			this.gameObject.GetComponent<Image>().sprite = cyclops;
			break;
		case(7):
			this.gameObject.GetComponent<Image>().sprite = demon;
			break;
		case(8):
			this.gameObject.GetComponent<Image>().sprite = deathknight;
			break;
		case(9):
			this.gameObject.GetComponent<Image>().sprite = dragon;
			break;
		default:
			Debug.Log ("Ce cas n'aurait pas du se produire ...");
			break;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		int numberOfTroop = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));
		switch(numberOfTroop)
		{
			case(1):
				if(Badguy.Instance.TotalMoney<10f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(2):
				if(Badguy.Instance.TotalMoney<50f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(3):
				if(Badguy.Instance.TotalMoney<70f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(4):
				if(Badguy.Instance.TotalMoney<150f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(5):
				if(Badguy.Instance.TotalMoney<200f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(6):
				if(Badguy.Instance.TotalMoney<300f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(7):
				if(Badguy.Instance.TotalMoney<500f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(8):
				if(Badguy.Instance.TotalMoney<650f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			case(9):
				if(Badguy.Instance.TotalMoney<2000f)
				{
					this.gameObject.GetComponent<Button>().interactable = false;
				}
				else
				{
					this.gameObject.GetComponent<Button>().interactable = true;
				}
				break;
			default:
				Debug.Log ("Ce cas n'aurait pas du se produire ...");
				break;
		}
	}
}
