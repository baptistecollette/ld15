/**
 * \file      ChangeToolTip.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de gérer une créature sur le terrain
 */

using UnityEngine;
using System.Collections;

public class MonsterOnTheMap : MonoBehaviour {

	public GameObject tile;

	private string monsterType;
	private Monster monster;

	public Sprite gobelin1;
	public Sprite gobelin2;
	public Sprite orc1;
	public Sprite orc2;
	public Sprite werewolf1;
	public Sprite werewolf2;
	public Sprite harpy1;
	public Sprite harpy2;
	public Sprite vampire1;
	public Sprite vampire2;
	public Sprite cyclops1;
	public Sprite cyclops2;
	public Sprite demon1;
	public Sprite demon2;
	public Sprite deathknight1;
	public Sprite deathknight2;
	public Sprite dragon1;
	public Sprite dragon2;


	private float counter;
	private int state;

	private float heightTile;
	private float widthTile;

	private bool goodSprite;

	private bool isDead;
	private float deathCounter;
	// Use this for initialization
	void Start () 
	{
		counter = 0;
		state = 1;

		widthTile = 2f * tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.x;
		heightTile = 2f * tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.y;

		goodSprite = false;

		deathCounter = 0;
		isDead = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(monster!=null && !isDead)
		{
			Sprite sprite1 = null;
			Sprite sprite2 = null;
			switch(monsterType)
			{
			case("Gobelin"):
				sprite1 = gobelin1;
				sprite2 = gobelin2;
				break;
			case("Orc"):
				sprite1 = orc1;
				sprite2 = orc2;
				break;
			case("Werewolf"):
				sprite1 = werewolf1;
				sprite2 = werewolf2;
				break;
			case("Harpy"):
				sprite1 = harpy1;
				sprite2 = harpy2;
				break;
			case("Vampire"):
				sprite1 = vampire1;
				sprite2 = vampire2;
				break;
			case("Cyclops"):
				sprite1 = cyclops1;
				sprite2 = cyclops2;
				break;
			case("Demon"):
				sprite1 = demon1;
				sprite2 = demon2;
				break;
			case("Deathknight"):
				sprite1 = deathknight1;
				sprite2 = deathknight2;
				break;
			case("Dragon"):
				sprite1 = dragon1;
				sprite2 = dragon2;
				break;
			default:
				
				break;
			}
			if(!goodSprite)
			{
				this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
				goodSprite = true;
			}
			counter += Time.deltaTime;

			if(counter>0.5f)
			{
				counter = 0;

				if(state == 1)
				{
					state = 2;
					this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
				}
				else
				{
					state = 1;
					this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
				}
			}

			if(!monster.Alive)
			{
				isDead = true;
			}
			float angle=0f;
			this.gameObject.transform.position = new Vector3(monster.PosY * widthTile,-monster.PosX * heightTile ,10f);
			if (monster.PosY>monster.TargetPosY){angle = 270f;}
			if (monster.PosY<monster.TargetPosY){angle = 90f;}
			if (monster.PosX>monster.TargetPosX){angle = 180f;}
			if (monster.PosX<monster.TargetPosX){angle = 0f;}
			this.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0f,0f,angle));
		}
		else if(isDead)
		{
			deathCounter += Time.deltaTime;
		}

		if(deathCounter>0.5f)
		{
			monster.GameObjectMonster = null;
			Destroy(this.gameObject);
		}
	}

	public void SetMonsterType(string _monsterType)
	{
		monsterType = _monsterType;
	}

	public string GetMonsterType()
	{
		return monsterType;
	}

	public void SetMonster(Monster _monster)
	{
		monster = _monster;
	}
	
	public Monster GetMonster()
	{
		return monster;
	}
}
