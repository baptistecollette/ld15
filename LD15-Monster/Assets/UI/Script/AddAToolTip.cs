﻿/**
 * \file      AddAToolTip.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet d'instancier un tooltip lorsqu'on survole un bouton
 */

using UnityEngine;
using System.Collections;

public class AddAToolTip : MonoBehaviour {


	private GameObject toolTip;
	public GameObject PrefabToolTip;

	// Use this for initialization
	void Start () 
	{
		//Instantiate the tooltip 
		toolTip = Instantiate (PrefabToolTip);
		//Set its parent and its position to (0,0)
		toolTip.transform.SetParent (this.gameObject.transform);
		toolTip.GetComponent<RectTransform> ().anchoredPosition -= toolTip.GetComponent<RectTransform> ().anchoredPosition;

		//Set its position to the right one
		float XPosition = this.gameObject.GetComponent<RectTransform> ().sizeDelta.x;
		toolTip.GetComponent<RectTransform> ().anchoredPosition -= new Vector2 (XPosition,0);

		toolTip.GetComponent<RectTransform> ().localScale = new Vector2 (1f, 1f);

		toolTip.SetActive (false);


	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void ShowToolTip()
	{

	}

	public GameObject GetToolTip()
	{
		return toolTip;
	}

	public void SetToolTip(GameObject _toolTip)
	{
		toolTip = _toolTip;
	}
}
