﻿/**
 * \file      CreateButtonTroopsCreation.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de créer la map avec les tiles
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateMap : MonoBehaviour {

	public GameObject PrefabLand;
	public GameObject PrefabRoad;
	public GameObject PrefabTown;
	public GameObject PrefabSpawn;

	private int compteur;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public GameObject InstantiateTile(int row, int column, TileType type, Tile actualTile)
	{
		//Create an empty GameObject
		GameObject tile;
		//Instantiate the good type of tile
		switch(type)
		{
		case(TileType.LAND):
			tile = Instantiate(PrefabLand);
			break;
		case(TileType.ROAD):
			tile = Instantiate(PrefabRoad);
			break;
		case(TileType.START):
			tile = Instantiate(PrefabSpawn);
			break;
		case(TileType.VILLAGE):
			tile = Instantiate(PrefabTown);
			break;
		default:
			tile = null;
			Debug.Log ("Encore un cas qui ne devrait pas arriver ... Un TileType de ce type n'existe pas");
			break;
		}

		//Set the parents and reset the position to (0,0,0)
		tile.transform.SetParent (this.transform);
		tile.transform.position -= tile.transform.position;

		//Get the width and height of the tile
		float width = 2f*tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.x;
		float height = 2f*tile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.y;

		//Set the position according to row and column
		tile.transform.position += new Vector3 ((float)column * width,-(float)row * height,10f);

		//Create the link between the C# tile and the GameObject
		tile.GetComponent<LinkTileGameObject> ().SetTile (actualTile);
		tile.GetComponent<LinkTileGameObject> ().SetRow (row);
		tile.GetComponent<LinkTileGameObject> ().SetColumn (column);

		return tile;
	}
}
