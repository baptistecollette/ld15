﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeCounter : MonoBehaviour {

	private int numberOfRoad;

	// Use this for initialization
	void Start () 
	{
		numberOfRoad = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch(numberOfRoad)
		{
		case(1):
			this.gameObject.GetComponent<Text>().text = Mathf.Max(0,GameManager.Instance.LifeUpperRoad()).ToString();
			break;
		case(2):
			this.gameObject.GetComponent<Text>().text = Mathf.Max(0,GameManager.Instance.LifeLeftRoad()).ToString();
			break;
		case(3):
			this.gameObject.GetComponent<Text>().text = Mathf.Max(0,GameManager.Instance.LifeRightRoad()).ToString();
			break;
		case(4):
			this.gameObject.GetComponent<Text>().text = Mathf.Max(0,GameManager.Instance.LifeDownRoad()).ToString();
			break;
		default:
			Debug.Log ("Problème");
			break;
		}
	}
}
