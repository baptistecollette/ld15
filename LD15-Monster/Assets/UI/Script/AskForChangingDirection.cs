﻿/**
 * \file      AskForChangingDirection.cs
 * \author    Amaury Depierre
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Permet de cdemander à changer la direction dans laquelle les monstres sont envoyés
 */

using UnityEngine;
using System.Collections;

public class AskForChangingDirection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AskForChangement()
	{
		int direction = int.Parse (this.gameObject.name.Substring (this.gameObject.name.Length - 1));
		GameObject.Find ("MenuChoiceDirection").GetComponent<ChangeDirection> ().ChangeTheDirection (direction);
	}
}
