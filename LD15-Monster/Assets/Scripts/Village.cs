/**
 * \file      Village.cs
 * \author    Baptiste Collette
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Contains the Villages and all towers.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public enum TowerTypes {
	ARROW_TOWER,
	CANNON_TOWER,
	ICE_TOWER,
	FIRE_TOWER,
	DEATH_TOWER,
	NULL_TOWER
}

public class Village {
	Dictionary<TowerTypes,int> costTowers = new Dictionary<TowerTypes, int>(){};
	int pacing = 0; //1 chance out of pacing to build a tower each time it wants to.
	float timeBetweenTwoConstructionTriesInSeconds = 20;
	float timeBeforeNextTryToConstruct;

	int income = 1000;
	public int Income {get{return income;} set{income=value;}}
	int money = 100000;
	public int Money {get{return money;} set{money=value;}}
	bool alive = true; //false when life is 0
	public bool Alive {get{return alive;} set{alive=value;}}
	int life = 1; //How many enemies need to enter the village for it to fall
	public int Life {get{return life;} set{life=value;}}
	Road road;
	public Road Road {get{return road;} set{road=value;}}
	List<Tower> towers = new List<Tower>(){};
	public void AddTower (Tower t){
		if(!towers.Contains(t)) 
		{
			towers.Add (t);
			t.Village=this;
//			Debug.Log ("Adding tower "+t.TowerType+" at "+t.Tile.PositionXOnMap+","+t.Tile.PositionYOnMap);
		}
	}
	public void RemoveTower (Tower t){if(towers.Contains(t)) towers.Remove (t);}
	int posX = 0;
	public int PosX {get{return posX;} set{posX=value;}}
	int posY = 0;
	public int PosY {get{return posY;} set{posY=value;}}
	float timeBetweenTwoIncomes = 5f;
	public float TimeBetweenTwoIncomes {get{return timeBetweenTwoIncomes;} set{timeBetweenTwoIncomes=value;}}
	float timeBeforeNextIncome;
	public float TimeBeforeNextIncome {get {return timeBeforeNextIncome;} set {timeBeforeNextIncome=value;}}

	public Village (Road r, int x, int y)
	{
		//Dictionary
		costTowers[TowerTypes.DEATH_TOWER]=1200;
		costTowers[TowerTypes.FIRE_TOWER]=600;
		costTowers[TowerTypes.ICE_TOWER]=400;
		costTowers[TowerTypes.CANNON_TOWER]=300;
		costTowers[TowerTypes.ARROW_TOWER]=100;

		switch (GameManager.Instance.Difficulty)
		{
			case Difficulty.EASY:
				income = 100;
				life = 20;
				money = 200;
				timeBetweenTwoConstructionTriesInSeconds = 10;
				timeBetweenTwoIncomes = 20;
				break;
			case Difficulty.MEDIUM:
				income = 250;
				life=40;
				money = 400;
				timeBetweenTwoConstructionTriesInSeconds = 10;
				timeBetweenTwoIncomes = 10;
				break;
			case Difficulty.HARD:
				income = 500;
				life=60;	
				money = 1200;
				timeBetweenTwoConstructionTriesInSeconds = 10;
				timeBetweenTwoIncomes = 5;
				break;
			default:
				Debug.LogWarning("Unknown difficulty : "+GameManager.Instance.Difficulty);
				break;
		}

		r.Village=this;
		road=r;
		posX=x;
		posY=y;
		CreateTower();
		timeBeforeNextTryToConstruct = timeBetweenTwoConstructionTriesInSeconds;
	}
	public Village (Road r, int x, int y, int life_) : this(r,x,y)
	{
		life=life_;
	}

	public void ChangeLife (int delta)
	{
		if(alive)
		{
			life+=delta;
			//Debug.Log ("Village a perdu de la vie");
			if(life<1) //Village is down, remove the road
			{
				foreach (Tower t in towers)
				{
					t.Collapse();
				}
				alive=false;
				road.VillageDown();
			}
		}
	}

	public void GetIncome ()
	{
		AddMoney(income);
	}
	
	public void ChangeIncome (int delta)
	{
		income+=delta;
	}

	public bool Pay(int cost)
	{
		if(money>=cost)
		{
			money-=cost;
			return true;
		}
		else
			return false;
	}

	public void PassTime (float deltaTime)
	{
		timeBeforeNextIncome -= deltaTime;
		if(timeBeforeNextIncome<=0)
		{
			timeBeforeNextIncome=timeBetweenTwoIncomes;
			GetIncome();
		}
		foreach(Tower t in towers)
		{
			t.PassTime(deltaTime);
		}
		timeBeforeNextTryToConstruct-=deltaTime;

		if(timeBeforeNextTryToConstruct<=0)
		{
			timeBeforeNextTryToConstruct=timeBetweenTwoConstructionTriesInSeconds; //Set to max
			CreateTower();
		}
	}

	public void AddMoney (int delta)
	{
		Money+=delta;
		Money=Mathf.Max (Money,0);
	}

	public void CreateTower ()
	{
		if(Random.Range (0,pacing)>0) //Stop - pacing
			if(GameManager.Instance.Difficulty==Difficulty.EASY || costTowers[TowerTypes.DEATH_TOWER]>money) //If not easy, the village will always try to build a Death tower or better even if pacing.
				return;

		//Find best tile
		Tile bestTile;
		List<Tile> bestTiles = new List<Tile>(){};
		int maxAdjacentTiles = 0;
		foreach (Tile t in road.Map.Tiles)
		{
			if(t.TileType==TileType.LAND && t.Tower==null)
			{
				int adjacentTiles = 0;
				if(t.posUpTile()!=null && road.Tiles.Contains( t.posUpTile()))
				{
					adjacentTiles++;
				}
				if(t.posDownTile()!=null && road.Tiles.Contains( t.posDownTile()))
				{
					adjacentTiles++;
				}
				if(t.posRightTile()!=null && road.Tiles.Contains( t.posRightTile()))
				{
					adjacentTiles++;
				}
				if(t.posLeftTile()!=null && road.Tiles.Contains( t.posLeftTile()))
				{
					adjacentTiles++;
				}
				if(adjacentTiles==maxAdjacentTiles)
				{
					bestTiles.Add (t);
				}
				if(adjacentTiles>maxAdjacentTiles)
				{
					maxAdjacentTiles=adjacentTiles;
					bestTiles = new List<Tile>(){t};
				}
			}
		}
		if(bestTiles.Count==0 || maxAdjacentTiles==0)
		{
			//Consider removing a bad tower
			bestTiles = new List<Tile>(){};
			float worstTowers = 0;

			foreach (Tower tow in towers)
			{
				int type = costTowers[tow.TowerType];
				int adjacentTiles = 0;
				Tile t = tow.Tile;
				if(t.posUpTile()!=null && road.Tiles.Contains( t.posUpTile()))
				{
					adjacentTiles++;
				}
				if(t.posDownTile()!=null && road.Tiles.Contains( t.posDownTile()))
				{
					adjacentTiles++;
				}
				if(t.posRightTile()!=null && road.Tiles.Contains( t.posRightTile()))
				{
					adjacentTiles++;
				}
				if(t.posLeftTile()!=null && road.Tiles.Contains( t.posLeftTile()))
				{
					adjacentTiles++;
				}
				float howBadIsTheTower = (float)adjacentTiles/(float)type;

				if(howBadIsTheTower==worstTowers)
				{
					bestTiles.Add (t);
				}
				if(howBadIsTheTower>worstTowers)
				{
					worstTowers=howBadIsTheTower;
					bestTiles = new List<Tile>(){t};
				}
			}
			if(bestTiles.Count==0 || worstTowers==0)
			{
				Debug.Log ("Nothing to do");
				return; //Nothing to do
			}
			else
			{
				bestTile = bestTiles[Random.Range (0,bestTiles.Count-1)];
				if(bestTile.Tower!=null)
					bestTile.Tower.Collapse(); //It will remove bestTile's tower.
			}
		}
		else
			bestTile = bestTiles[Random.Range (0,bestTiles.Count-1)];
			
		//Best tile exists, look for the best tower - the one that costs the most.
		//TODO MANUALLY ADD ALL TOWERS
		if(Pay (costTowers[TowerTypes.DEATH_TOWER]))
		{			
			pacing++;
			AddTower(new DeathTower(bestTile,this));
		}
		else if(Pay (costTowers[TowerTypes.FIRE_TOWER]))
		{
			pacing++;
			AddTower(new FireTower(bestTile,this));
		}
		else if(Pay (costTowers[TowerTypes.ICE_TOWER]))
		{
			pacing++;
			AddTower(new IceTower(bestTile,this));
		}
		else if(Pay (costTowers[TowerTypes.CANNON_TOWER]))
		{
			pacing++;
			AddTower(new CannonTower(bestTile,this));
		}
		else if(Pay (costTowers[TowerTypes.ARROW_TOWER]))
		{
			pacing++;
			AddTower(new ArrowTower(bestTile,this));
		}
	}
}

//---------------------------------------------------------------------------------------------------//
// TOWERS //

public abstract class Tower
{
	TowerTypes towerType = TowerTypes.NULL_TOWER;
	public TowerTypes TowerType {get {return towerType;} set{towerType=value;}}
	bool active = true;
	public bool Active {get {return active;} set {active=value;}}
	Tile tile;
	public Tile Tile {get {return tile;} set {tile=value;}}
	float life = 100; //If it breaks
	public float Life {get {return life;} set {life=value;}}
	bool flyAttack = true; //If it can attack flying creatures
	public bool FlyAttack {get {return flyAttack;} set {flyAttack=value;}}
	float damage = 1f; //How much damage per attack
	public float Damage {get {return damage;} set {damage=value;}}
	float range = 1f;
	public float Range {get {return range;} set{range=value;}}
	float timeBetweenTwoAttacksInSeconds = 0.2f;
	public float TimeBetweenTwoAttacksInSeconds {get{return timeBetweenTwoAttacksInSeconds;} set{timeBetweenTwoAttacksInSeconds=value;}} 
	float timeBeforeNextTryToAttack;
	public float TimeBeforeNextTryToAttack {get {return timeBeforeNextTryToAttack;} set {timeBeforeNextTryToAttack=value;}}

	Map map;
	public Map Map {get {return map;} set {map=value;}}
	Road road;
	public Road Road {get {return road;} set {road=value;}}
	Village village;
	public Village Village {get {return village;} set {village=value;}}
	bool splash = false;
	public bool Splash {get {return splash;} set {splash=value;}}
	float splashRadius = 0f;
	public float SplashRadius {get {return splashRadius;} set {splashRadius=value;}}
	bool slowFoes = false; //True if slows down monsters
	public bool SlowFoes {get {return slowFoes;} set{slowFoes=value;}}
	float timeSlow = 2; //How many seconds of slow
	public float TimeSlow {get{return timeSlow;}set{timeSlow=value;}}
	float slowFactor = 1; //By how much is the monster slowed down. Positive : makes it faster
	public float SlowFactor {get{return slowFactor;} set{slowFactor=value;}}
	GameObject prefabTower = Resources.Load<GameObject> ("Prefabs/Towers") as GameObject;
	GameObject gameObjectTower;

	GameObject projectile = Resources.Load<GameObject>("Prefabs/Projectiles") as GameObject;

	public Tower (TowerTypes tt,Tile tile_, float life_, float damage_, float range_, float timeBetweenTwoAttacksInSeconds_, Village village_)
		:this(tt,tile_,life_,damage_,range_,timeBetweenTwoAttacksInSeconds_,village_,0,0,0)
	{
	}

	public Tower (TowerTypes tt,Tile tile_, float life_, float damage_, float range_, float timeBetweenTwoAttacksInSeconds_, Village village_, float splashRadius_, float timeSlow_, float slowFactor_)
	{
		timeSlow=timeSlow_;
		slowFactor = slowFactor_;
		slowFoes=timeSlow>0;
		splashRadius=splashRadius_;
		splash=splashRadius>0;
		towerType=tt;
		tile=tile_;
		tile.Tower=this;
		life=life_;
		damage=damage_;
		timeBetweenTwoAttacksInSeconds=timeBetweenTwoAttacksInSeconds_;
		range=range_;
		village=village_;
		road=village.Road;
		map=road.Map;
		timeBeforeNextTryToAttack = timeBetweenTwoAttacksInSeconds;

		GameObject theNewTower = GameObject.Instantiate (prefabTower);

		theNewTower.GetComponent<TowerOnTheMap> ().SetTower (this);

		gameObjectTower = theNewTower;
	}

	public void TakeDamage (float damage)
	{
		Life-=damage;
		if(Life<0)
		{
			Collapse();
		}
	}

	public void Collapse ()
	{
		active = false;
		Village.RemoveTower(this);
		tile.RemoveTower();

		GameObject.Destroy (gameObjectTower);
	}

	/**
	 * @brief Tries to attack the nearest enemy, returns false if couldn't reach anyone
	 */
	public bool Attack ()
	{
		List<Monster> monsters = road.MonstersInRoad;
		if(monsters.Count==0)
		{
			return false;
		}
//		Debug.Log ("Found enemy on road");
		monsters = monsters.OrderBy(monster=>
		                            (monster.PosX-tile.PositionXOnMap)*(monster.PosX-tile.PositionXOnMap)+
		                            (monster.PosY-tile.PositionYOnMap)*(monster.PosY-tile.PositionYOnMap))
			.ToList();

		Monster bestTarget = null;
		if(FlyAttack)
			bestTarget = monsters[0];
		else //Look through all monsters to find the closest non-flying one
		{
			int remainingMonsters = monsters.Count;
			while (bestTarget==null && remainingMonsters>0)
			{
				if(!monsters[monsters.Count-remainingMonsters].Flying) {
					bestTarget=monsters[monsters.Count-remainingMonsters];
					break;
				}
				else
					remainingMonsters--;
			}
		}
		if(bestTarget==null)
			return false;
		if((bestTarget.PosX-tile.PositionXOnMap)*(bestTarget.PosX-tile.PositionXOnMap)+
		   (bestTarget.PosY-tile.PositionYOnMap)*(bestTarget.PosY-tile.PositionYOnMap) <= Range*Range)
		{
			GameObject newProjectile = GameObject.Instantiate(projectile);
			ProjectileOnMap projectileOnMap = newProjectile.GetComponent<ProjectileOnMap>();

			projectileOnMap.SetTarget(bestTarget.GameObjectMonster);
			projectileOnMap.SetStartPosition(this.gameObjectTower.transform.position);
			projectileOnMap.SetTypeTower(this.towerType);

			bestTarget.RemoveLife(Damage);
			if(slowFoes)
			{
				bestTarget.Slow(slowFactor,timeSlow);
			}
			int treasureIfTargetDown = Mathf.Abs(Mathf.FloorToInt(bestTarget.Cost/10)); //How much money the village gets if the tower kills it
			if (bestTarget.RemoveLife(Damage))
			{
				Village.AddMoney(treasureIfTargetDown);
			}
			if(splash)
			{
				float x = bestTarget.PosX;
				float y = bestTarget.PosY;
				foreach (Monster m in Badguy.Instance.Monsters)
				{
					if(m!=bestTarget) {
						float xMonster = m.PosX-x;
						float yMonster = m.PosY-y;
						float distance2 = (xMonster)*(xMonster)+yMonster*yMonster;
						float percentage = distance2/splashRadius/splashRadius;
						if(percentage<=1)
						{
							treasureIfTargetDown = Mathf.Abs(Mathf.FloorToInt(m.Cost/10));
							if(slowFoes)
							{
								m.Slow(slowFactor,timeSlow);
							}
							if(m.RemoveLife(percentage*Damage))
							{
								Village.AddMoney(treasureIfTargetDown);
							}
						}
					}
				}
			}
//			Debug.Log ("Attacked");
			return true;
		}
		return false;
	}

	public void PassTime (float passTime)
	{
		timeBeforeNextTryToAttack -= passTime;
		if(timeBeforeNextTryToAttack<=0)
		{ //Try to attack
			if(Attack ())
			{
				timeBeforeNextTryToAttack=timeBetweenTwoAttacksInSeconds;
			}
			else
			{
				timeBeforeNextTryToAttack=timeBetweenTwoAttacksInSeconds/5;
			}
		}
	}
}

public class FireTower : Tower
{
	public FireTower(Tile tile_, Village village_) : base (TowerTypes.FIRE_TOWER,tile_,100f,10f,2f,0.3f,village_,1f,0f,0f)
	{

	}
}

public class ArrowTower : Tower
{
	public ArrowTower(Tile tile_, Village village_) : base (TowerTypes.ARROW_TOWER,tile_,100f,5f,3f,1f,village_,0.01f,0f,0f)
	{
		
	}
}

public class CannonTower : Tower
{
	public CannonTower(Tile tile_, Village village_) : base (TowerTypes.CANNON_TOWER,tile_,100f,15f,4f,2.5f,village_,1.5f,0f,0f)
	{
		
	}
}

public class DeathTower : Tower
{
	public DeathTower(Tile tile_, Village village_) : base (TowerTypes.DEATH_TOWER,tile_,100f,100f,5f,2f,village_,0.1f,0f,0f)
	{
		
	}
}

public class IceTower : Tower
{
	public IceTower(Tile tile_, Village village_) : base (TowerTypes.ICE_TOWER,tile_,100f,20f,3f,1.5f,village_,0.5f,2f,0.5f)
	{
		
	}
}