﻿/**
 * \file      Playground.cs
 * \author    Baptiste Collette
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Contains all Tiles, Map and Road description. Map loading takes place here.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TileType
{
	LAND,
	ROAD,
	VILLAGE,
	START
}

public class Tile
{
	Tower tower; //Tile is linked when tower is created
	public Tower Tower {get {return tower;} set{tower=value;}}
	Map map;
	public Map Map {get {return map;} set{map=value;}}
	TileType tileType;
	public TileType TileType {get {return tileType;} set{tileType=value;}}
	int positionXOnMap = 0;
	public int PositionXOnMap {get {return positionXOnMap;} set {positionXOnMap=value;}}
	int positionYOnMap = 0;
	public int PositionYOnMap {get {return positionYOnMap;} set {positionYOnMap=value;}}
	GameObject tileGameObject;
	public GameObject TileGameObject {get {return tileGameObject;} set {tileGameObject=value;}}

	public Tile()
	{
	}
	public Tile(Map m,TileType tt, int x, int y)
	{
		Map=m;
		m.AddTile(this);
		TileType=tt;
		PositionXOnMap=x;
		PositionYOnMap=y;
		//DONE
		tileGameObject = GameObject.Find ("Map").GetComponent<CreateMap> ().InstantiateTile (x,y,tt,this);
	}
	public void RemoveTower()
	{
		tower=null;
	}
	public Tile posUpTile () {return map.findTileByInt(map.posUpTile (PositionYOnMap+PositionXOnMap*map.NumberOfColumns));}
	public Tile posDownTile () {return map.findTileByInt(map.posDownTile (PositionYOnMap+PositionXOnMap*map.NumberOfColumns));}
	public Tile posLeftTile () {return map.findTileByInt(map.posLeftTile (PositionYOnMap+PositionXOnMap*map.NumberOfColumns));}
	public Tile posRightTile () {return map.findTileByInt(map.posRightTile (PositionYOnMap+PositionXOnMap*map.NumberOfColumns));}
}

public class TileRoad : Tile
{
	Road road;
	public Road Road {get {return road;} set{road=value;}}
	int positionTileInRoad = 0;
	public int PositionTileInRoad {get {return positionTileInRoad;} set {positionTileInRoad=value;}}

	TileRoad parent;
	public TileRoad Parent {get {return parent;} set {parent=value;}}
	TileRoad child; //No child : the next tile is the village set on the road
	public TileRoad Child {get {return child;} set {child=value;}}

	public TileRoad(){}
	public TileRoad (Map m, TileRoad parent_, int x, int y) : base(m,TileType.ROAD,x,y)
	{
		if(parent_!=null)
		{
			parent=parent_;
			parent.Child=this;
			PositionTileInRoad = parent.PositionTileInRoad+1;
		}
		else
		{
			PositionTileInRoad = 0;
		}
	}
	public TileRoad (Map m, TileRoad parent_, int x, int y, TileType tt) : base (m,tt,x,y)
	{
		if(parent_!=null)
		{
			parent=parent_;
			parent.Child=this;
			PositionTileInRoad = parent.PositionTileInRoad+1;
		}
		else
		{
			PositionTileInRoad = 0;
		}
	}
}

public class TileStart : TileRoad
{
	public TileStart (){}
	public TileStart(Map m, int x, int y) : base(m,null,x,y,TileType.START)
	{
		PositionTileInRoad = 0;
	}
}

public class Map {
	List<Village> villages = new List<Village>(){};
	List<Tile> tiles = new List<Tile>(){};
	public List<Tile> Tiles {get{return tiles;}}
	public Tile findTileByInt (int pos)
	{
		int x = pos/numberOfColumns;
		int y = pos%numberOfColumns;
		foreach(Tile t in tiles)
		{
			if(t.PositionXOnMap==x && t.PositionYOnMap==y)
				return t;
		}
		return null;
	}

	List<Road> roads = new List<Road>(){};
	public List<Road> Roads {get{return roads;} set{roads=value;}}
	int numberOfColumns = 0;
	public int NumberOfColumns {get {return numberOfColumns;} set {numberOfColumns=value;}}
	int numberOfLines = 0;
	public int NumberOfLines {get {return numberOfLines;} set {numberOfLines=value;}}
	int posStart = 0;
	public int PosStart {get {return posStart;} set {posStart=value;}}
	int numberOfTiles = 0;
	public int NumberOfTiles {get {return numberOfTiles;} set {numberOfTiles=value;}}

	public void AddTile (Tile t) {if (!tiles.Contains(t)) tiles.Add (t);}
	public void AddRoad (Road r) {roads.Add (r);r.Map=this;}
	public void AddVillage (Village v) {if (!villages.Contains(v)) villages.Add (v);}

	/**
	 * @brief Reads a file and creates the map.
	 * @details 0 LAND ; 1 ROAD ; 3 START ; Villages are at the end of the roads.
	 */
	public void ReadFile (string path)
	{
		TileStart ts = new TileStart();
		List<int> infos=new List<int>(){};
		numberOfColumns = 0;
		numberOfLines = 0;
		posStart = 0;
		numberOfTiles = 0;

		//System.IO.StreamReader file = new System.IO.StreamReader(path);
		TextAsset tx = Resources.Load (GameManager.Instance.Directory+GameManager.Instance.FileName) as TextAsset;
		foreach(string line in tx.text.Split("\n"[0]))
		{
			if(line.Contains(";")) {
				numberOfLines++;
				numberOfColumns=0;
				foreach (string word in line.Split(';')) {
					numberOfColumns++;
					int a =int.Parse(word);
					infos.Add(a);
					if(a==3){
						ts = new TileStart(this,numberOfLines-1,numberOfColumns-1);
						posStart = numberOfTiles;
					}
					if(a==0){
						new Tile(this,TileType.LAND,numberOfLines-1,numberOfColumns-1);
					}
					numberOfTiles++;
				}
			}
		}
//		file.Close();

		if(infos.Count==0)
			return;

#if DEBUG
		string test = "";
		for(int i=0; i<infos.Count;i++)
		{
			test+=infos[i]+" ";
			if (((i+1) % numberOfColumns)==0)
				test+="\n";
		}
#endif

		//Up road
		int currentPos = posStart;
		List<int> nextTiles = new List<int>(){
			posUpTile (posStart),
			posLeftTile (posStart),
			posRightTile (posStart),
			posDownTile (posStart)
		};
		//List<string> posForDebug = new List<string>(){"Up","Left","Right","Down"};

		for (int posNextTiles=0;posNextTiles<nextTiles.Count;posNextTiles++)
		{
			int nextTile = nextTiles[posNextTiles];
			TileRoad currentTile = ts;
			if(nextTile>-1) //There is an upper road. Create it.
			{
				Road upperRoad = new Road();
				upperRoad.AddTile (currentTile);
				upperRoad.StartTile=currentTile;
				bool villageReached = false;
				int maxIter = 100;
				while(!villageReached && maxIter>0)
				{
					maxIter--;
					//Look if it is the village
					bool village = true;
					//Look if there is a new road up
					List<int> listI = new List<int>(){
						posUpTile (nextTile),
						posDownTile (nextTile),
						posLeftTile(nextTile),
						posRightTile(nextTile)
					};
					for (int j=0; j<listI.Count; j++)
					{
						int n=listI[j];
						if(n>-1 && infos[n]==1 && n!=currentPos) //the road continues
						{
							currentTile = new TileRoad(this,currentTile,nextTile/numberOfColumns,nextTile%numberOfColumns);
							currentPos = nextTile;
							upperRoad.AddTile(currentTile);
							if(upperRoad.SecondTile==null)
								upperRoad.SecondTile=currentTile;
							nextTile=n;
							village = false;
							break;
						}
					}
					if(village)
					{
						currentTile = new TileRoad(this,currentTile,nextTile/numberOfColumns,nextTile%numberOfColumns,TileType.VILLAGE);
						upperRoad.AddTile (currentTile);
						if(upperRoad.SecondTile==null)
							upperRoad.SecondTile=currentTile;
						AddRoad(upperRoad);
						villageReached = true;
						Village upperRoadVillage = new Village(upperRoad,nextTile/numberOfColumns,nextTile%numberOfColumns);
						AddVillage(upperRoadVillage);
					}
				}
			}
			else
				AddRoad(null);
		}
	}

	/**
	 * @brief Pass time for all villages. Conveniently returns true if all villages are dead.
	 */
	public bool PassTimeAllVillages (float deltaTime)
	{
		bool areAllVillagesDead = true;
		foreach(Village v in villages)
		{
			if(v.Alive) {
				areAllVillagesDead = false;
				v.PassTime(deltaTime);
			}
		}
		return areAllVillagesDead;
	}

	public int posUpTile (int tile) {int res = tile-numberOfColumns;if(res<0||res>numberOfColumns*numberOfLines)return -1;else return res;}
	public int posDownTile (int tile) {int res = tile+numberOfColumns;if(res<0||res>numberOfColumns*numberOfLines)return -1;else return res;}
	public int posLeftTile (int tile) {int res = tile-1;if((tile%numberOfColumns)==0)return -1;else return res;}
	public int posRightTile (int tile) {int res = tile+1;if((tile%numberOfColumns)==(numberOfColumns-1))return -1;else return res;}
}

public class Road
{
	Map map;
	public Map Map {get{return map;} set{map=value;}}
	TileRoad startTile;
	public TileRoad StartTile {get{return startTile;} set{startTile=value;}}
	TileRoad secondTile;
	public TileRoad SecondTile {get{return secondTile;} set{secondTile=value;}} //So that we can go on the right road from the start.
	List<Tile> tiles = new List<Tile>(){};
	public List<Tile> Tiles {get{return tiles;}}
	Village village;
	public Village Village {set{village=value;} get{return village;}}
	List<Monster> monstersInRoad = new List<Monster>(){};
	public List<Monster> MonstersInRoad {get{return monstersInRoad;}}
	public void AddTile (Tile t) {if (!tiles.Contains(t)) tiles.Add (t);}
	public void AddMonster (Monster m) {monstersInRoad.Add (m);}
	public void RemoveMonster (Monster m) {monstersInRoad.Remove(m);}

	public bool IsVillageAlive ()
	{
		return Village.Alive;
	}

	public int GetVillageLife ()
	{
		return Village.Life;
	}

	/**
	 * @brief Called by village when it's down.
	 */
	public void VillageDown()
	{
		GameObject prefabFlame = Resources.Load<GameObject> ("Prefabs/Flame") as GameObject;
		GameObject prefabTile = Resources.Load<GameObject> ("Tiles/Land") as GameObject;
		
		float width = 2f * prefabTile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.x;
		float height = 2f * prefabTile.gameObject.GetComponent<SpriteRenderer> ().bounds.max.y;
		
		//Set the position according to row and column
		GameObject.Instantiate(prefabFlame,new Vector3((float)village.PosY * width,-(float)village.PosX * height,10f),Quaternion.identity);
		Debug.Log ("Village down");
		foreach (Monster m in monstersInRoad)
		{
			m.Kill();
		}
		foreach (Tile t in tiles)
		{
			t.TileType=TileType.LAND; //The road is destroyed
		}
	}
}