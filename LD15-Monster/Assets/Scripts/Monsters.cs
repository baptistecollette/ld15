/**
 * \file      Monsters.cs
 * \author    Baptiste Collette
 * \version   1.0
 * \date      11 Novembre 2015
 * \brief     Contains all Monsters and Badguy (player).
 */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class Badguy
{
	private static volatile Badguy instance;
	private static object syncRoot = new Object();
	int income = 0; //How much money you get each turn
	public int Income {get{return income;} set{income=value;}}
	int totalMoney = 1000; //How much money you have
	public int TotalMoney {get{return totalMoney;} set{totalMoney=value;}}
	List<Monster> monsters = new List<Monster>(){};
	public List<Monster> Monsters {get{return monsters;}}
	public void AddMonster(Monster m) {if(!monsters.Contains(m)) monsters.Add (m);}
	public void RemoveMonster(Monster m) {if(monsters.Contains(m)) monsters.Remove (m);}
	float timeBetweenTwoIncomes = 5;
	float timeLeftBeforeNextIncome;

	public Badguy ()
	{
		timeLeftBeforeNextIncome=timeBetweenTwoIncomes;
		switch (GameManager.Instance.Difficulty)
		{
		case Difficulty.EASY:
			totalMoney = 100;
			timeBetweenTwoIncomes = 5;
			break;
		case Difficulty.MEDIUM:
			totalMoney = 50;
			timeBetweenTwoIncomes = 5;
			break;
		case Difficulty.HARD:
			totalMoney = 50;
			timeBetweenTwoIncomes = 10;
			break;
		}
	}
	public static Badguy Instance
	{
		get
		{
			if (instance == null)
			{
				lock (syncRoot)
				{
					if (instance == null)
						instance = new Badguy();
				}
			}
			
			return instance;
		}
	}
	public void ResetBadguy ()
	{
		lock (syncRoot)
		{
			if (instance == null)
				instance = new Badguy();
		}
	}

	public bool RemoveMoney (int cost)
	{
		if(totalMoney>=cost)
		{
			totalMoney-=cost;
//			Debug.Log ("Cash after monster : "+totalMoney);
			return true;
		}
		return false;
	}
	public void ChangeIncome (int delta)
	{
//		Debug.Log ("Change income");
		income+=delta;
		income=Mathf.Max (0,income);
	}
	public void GetIncome ()
	{
		totalMoney+=income;
		if(totalMoney<0) totalMoney=0;
		Debug.Log ("Cash : "+totalMoney);
	}
	public void PassTime (float deltaTime)
	{
//		Debug.Log (timeLeftBeforeNextIncome);
		timeLeftBeforeNextIncome -= deltaTime;
		if(timeLeftBeforeNextIncome<=0)
		{
			timeLeftBeforeNextIncome=timeBetweenTwoIncomes;
			GetIncome();
		}
	}
}


//-------------------------------------------------------------------------//


public abstract class Monster {
	bool alive = true;
	public bool Alive {get {return alive;} set {alive=value;}}
	float speed = 0.5f; //How many tiles by second TODO
	public float Speed {get {return speed;} set {speed = value;}}
	float life = 10000f; //How much damage a unit can take
	public float Life {get {return life;} set {life=value;}}
	int income = 10; //Creating the unit gives a bonus or malus to the income
	public int Income {get {return income;} set {income=value;}}
	int cost = 100; //Creating the unit costs money
	public int Cost {get {return cost;} set {cost=value;}}
	float attack = 0; //How much damage a unit deals to a tower
	public float Attack {get {return attack;} set {attack=value;}}
	int lifeCostVillage = 1; //If the unit enters a village, how many life points it loses => >=0
	public int LifeCostVillage {get {return lifeCostVillage;} set {lifeCostVillage=value;}}
	bool attacksTowers = false; //The unit first tries to destroy towers and then goes to village
	public bool AttacksTowers {get {return attacksTowers;} set {attacksTowers=value;}}
	Road road; //Which road the monster is on
	public Road Road {get {return road;} set {road=value;}}
	TileRoad currentTile; //The ID of the current tile. Non effective if flying creature.
	public TileRoad CurrentTile {get {return currentTile;} set {currentTile=value;}}
	float posX = -1; //Current position
	public float PosX {get {return posX;} set {posX=value;}}
	float targetPosX = -1; //Next tile's position
	public float TargetPosX {get {return targetPosX;} set {targetPosX=value;}}
	float posY = -1;
	public float PosY {get {return posY;} set {posY=value;}}
	float targetPosY = -1;
	public float TargetPosY {get {return targetPosY;} set {targetPosY=value;}}
	bool flying = false; //If the unit is flying
	public bool Flying {get {return flying;} set {flying=value;}}
	float moveStep = 0.1f; //Common to all creatures !
	public float MoveStep {get{return moveStep;} set{moveStep=value;}}
	float speedMult = 1f; //Changes when slow
	public float SpeedMult {get{return speedMult;}set{speedMult=value;}}
	float whenMakeSpeedMultNormal = 0f; //How long is the creature slowed down
	public float WhenMakeSpeedMultNormal {get{return whenMakeSpeedMultNormal;}set{whenMakeSpeedMultNormal=value;}}

	GameObject gameObjectMonster;
	public GameObject GameObjectMonster {get{return gameObjectMonster;} set{gameObjectMonster=value;}}


	protected Monster (float speed_, float life_, int income_, int cost_,Road r) : this(speed_,life_,income_,cost_,r,false,1,false,0)
	{

	}
	protected Monster (float speed_, float life_, int income_, int cost_, Road r,bool flying_, int lifeCostVillage_) : this(speed_,life_,income_,cost_,r,flying_,lifeCostVillage_,false,0)
	{
	}
	protected Monster (float speed_, float life_, int income_, int cost_, Road r,bool flying_, int lifeCostVillage_, bool attacksTowers_, float attack_)
	{
		Attack=attack_;
		AttacksTowers=attacksTowers_;
		Flying=flying_;
		LifeCostVillage=lifeCostVillage_;
		Speed=speed_;
		Life=life_;
		Income=income_;
		Cost=cost_;
		road=r;
		if(road.IsVillageAlive()) //true is village is alive
		{
			road.AddMonster(this);
			CurrentTile=r.StartTile;
			posX=r.StartTile.PositionXOnMap;
			posY=r.StartTile.PositionYOnMap;
			if(flying)
			{
				targetPosX=r.Village.PosX;
				targetPosY=r.Village.PosY;
				Debug.Log ("Flying : target "+targetPosX+","+targetPosY);
			}
			else
			{
				targetPosX=r.SecondTile.PositionXOnMap;
				targetPosY=r.SecondTile.PositionYOnMap;
			}
			if(Badguy.Instance.RemoveMoney(cost))
			{ //Create the instance
				Badguy.Instance.AddMonster(this);
				Badguy.Instance.ChangeIncome(income);
			}
			else
			{ //Kill it
				Kill ();
			}
		}
		else
		{
			Kill ();
		}
	}

	public void Slow (float slowFactor, float timeFactor)
	{
		SpeedMult = Mathf.Min (SpeedMult,slowFactor);
		WhenMakeSpeedMultNormal = Mathf.Max (timeFactor,WhenMakeSpeedMultNormal);
	}

	//Returns true if the monster is dead after the attack
	public bool RemoveLife (float delta)
	{
		life-=delta;
//		Debug.Log ("Lost "+delta+"HP down to "+life);
		if(life<=0)
		{
			Kill ();
			return true;
		}
		return false;
	}
	public void Kill ()
	{
		alive=false;
		Badguy.Instance.RemoveMonster(this);
		road.RemoveMonster(this);
		//TODO Remove sprite - Normally won't have to be done (?)
	}
	public void Move (float deltaTime)
	{
		MoveStep = deltaTime * speed;
		WhenMakeSpeedMultNormal-=deltaTime;
		if(WhenMakeSpeedMultNormal<0)
		{
			WhenMakeSpeedMultNormal=0;
			speedMult=1;
		}
		else
		{
			MoveStep*=speedMult;
		}
		if(alive)
		{
			if(Mathf.Abs(TargetPosX-PosX)<MoveStep && Mathf.Abs(TargetPosY-PosY)<MoveStep) 
			{ //Change target or die and damage village
				PosX=TargetPosX;
				PosY=TargetPosY;
				TileRoad newTile;
				if(CurrentTile == road.StartTile)
					newTile = road.SecondTile;
				else
					newTile = CurrentTile.Child;
				if(flying || newTile==null || newTile.Child==null || newTile.TileType==TileType.VILLAGE) //It's a village
				{ //If flying, is directly in the village
					road.Village.ChangeLife(-LifeCostVillage);
					Debug.Log ("Went to village");
					Kill ();
				}
				else
				{ //Not a village
					CurrentTile=newTile;
					Tile nextTarget = CurrentTile.Child;
					TargetPosX=nextTarget.PositionXOnMap;
					TargetPosY=nextTarget.PositionYOnMap;
				}
			}
			else
			{
				//Continue to move
				if(PosX!=TargetPosX)
					PosX+=MoveStep*Mathf.Sign(TargetPosX-PosX);
				if(posY!=TargetPosY)
					PosY+=MoveStep*Mathf.Sign(TargetPosY-PosY);
			}
		//	Debug.Log (PosX+","+PosY);
		}
	}
}

public class Goblin : Monster 
{
	private Goblin (Road r) : base(1f,10,1,10,r)
	{

	}
	public static Monster CreateMonster (Road r_)
	{
		return new Goblin (r_);
	}
}

public class Cyclops : Monster
{
	private Cyclops (Road r) : base(0.5f,500,20,300,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Cyclops (r_);
	}
}

public class Deathknight : Monster 
{
	private Deathknight (Road r) : base(0.7f,800,40,650,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Deathknight (r_);
	}
}

public class Demon : Monster 
{
	private Demon (Road r) : base(0.8f,600,20,500,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Demon (r_);
	}
}

public class Harpy : Monster 
{
	private Harpy (Road r) : base(0.8f,30,7,150,r,true,1)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Harpy (r_);
	}
}

public class Orc : Monster 
{
	private Orc (Road r) : base(0.6f,60,5,50,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Orc (r_);
	}
}

public class Vampire : Monster 
{
	private Vampire (Road r) : base(0.8f,250,14,200,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Vampire (r_);
	}
}

public class Werewolf : Monster 
{
	private Werewolf (Road r) : base(2.5f,30,5,70,r)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Werewolf (r_);
	}
}

public class Dragon : Monster 
{
	private Dragon (Road r) : base(0.4f,1500,-100,2000,r,true,1)
	{
		
	}
	public static Monster CreateMonster (Road r_)
	{
		return new Dragon (r_);
	}
}