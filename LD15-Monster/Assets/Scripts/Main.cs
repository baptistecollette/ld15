﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour {
	float startTime;
	float finishTime;
	int minutesGame = 30;
	bool gameNotOver = true;

	// Use this for initialization
	void Start () {
		//faire ici le truc avec la lecture de la base de données
		Map m = new Map();
		m.ReadFile(GameManager.Instance.Directory+GameManager.Instance.FileName);
		GameManager.Instance.UpperRoad = m.Roads[0];
		GameManager.Instance.LeftRoad = m.Roads[1];
		GameManager.Instance.RightRoad = m.Roads[2];
		GameManager.Instance.DownRoad = m.Roads[3];
		GameManager.Instance.Map = m;
		switch (GameManager.Instance.Difficulty)
		{
		case Difficulty.EASY:
			minutesGame=15;
			break;
		case Difficulty.MEDIUM:
			minutesGame=10;
			break;
		case Difficulty.HARD:
			minutesGame=5;
			break;
		}
		startTime = Time.time;
		finishTime = startTime + minutesGame*60;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if(!gameNotOver)
			return;
		if(finishTime<Time.time)
		{
			Debug.Log ("GIT GUD");
			gameNotOver = false;
			Application.LoadLevel ("Menu");
			//TODO : Lose
		}
		else
		{
			Badguy.Instance.PassTime(Time.fixedDeltaTime);
			List<Monster> monsters = Badguy.Instance.Monsters;
			foreach(Monster m in monsters)
			{
				if(m.Alive)
					m.Move(Time.fixedDeltaTime);
			}
//			Debug.Log (GameManager.Instance);
//			Debug.Log (GameManager.Instance.Map);
			bool isGameOver = GameManager.Instance.Map.PassTimeAllVillages(Time.fixedDeltaTime); //Checks the income for villages.
			if(isGameOver)
			{
				//TODO
				gameNotOver = false;
				Debug.Log ("WIN");
				Application.LoadLevel ("Menu");
			}
		}
		GameManager.Instance.TimeRemainingSeconds = Mathf.Max (0,Mathf.CeilToInt(finishTime-Time.time));
	}
}
