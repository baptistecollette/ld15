using UnityEngine;
using System.Collections;

public enum Difficulty
{
	EASY,
	MEDIUM,
	HARD
}

public sealed class GameManager
{
    private static volatile GameManager instance;
    private static object syncRoot = new Object();

	public static Difficulty difficulty = Difficulty.EASY;
	public Difficulty Difficulty {get{return difficulty;} set{difficulty=value;}}
	public static Road upperRoad;
	public Road UpperRoad {get{return upperRoad;} set{upperRoad=value;}}
	public static Road leftRoad;
	public Road LeftRoad {get{return leftRoad;} set{leftRoad=value;}}
	public static Road rightRoad;
	public Road RightRoad {get{return rightRoad;} set{rightRoad=value;}}
	public static Road downRoad;
	public Road DownRoad {get{return downRoad;} set{downRoad=value;}}
	public static string directory = "Maps/";
	public string Directory {get{return directory;}}
	public static string fileName = "map1";
	public string FileName {get{return fileName;} set{fileName=value;}}
	public static Map map;
	public Map Map {get{return map;} set{map=value;}}
	public static int timeRemainingSeconds;
	public int TimeRemainingSeconds {get{return timeRemainingSeconds;} set{timeRemainingSeconds=value;}}

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new GameManager();
                }
            }
            return instance;
        }
    }
	public int LifeUpperRoad () {return UpperRoad.GetVillageLife();}
	public int LifeDownRoad () {return DownRoad.GetVillageLife();}
	public int LifeLeftRoad () {return LeftRoad.GetVillageLife();}
	public int LifeRightRoad () {return RightRoad.GetVillageLife();}

}